package com.shashi.srv;

import com.shashi.utility.MathUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/unal/estudiantes")
public class NuevaFuncionalidadSrv extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        MathUtil.equals(2, 3);

        PrintWriter writer = response.getWriter();
        writer.println("Curso Gestion agil de proyectos");
        writer.println("Equipo:");
        writer.println(" - Juan Jose Duque Cardona");
        writer.println(" - Gustavo Andres Galvis Cifuentes");
        writer.println(" - Yidaido Rojas Barrantes");
        writer.println(" - Andras Grisales Gonzalez");
        writer.close();
        response.setStatus(200);
    }
}
